//Used for maintaining command container - can get, put and rollback

package basicDataStructure;

import java.util.ArrayList;

public class MotesSendComdListHandler {

	public int currentPosition = 0; // current command position
	private ArrayList<String> comdList = new ArrayList<String>();
	protected MotesSendComdListPositioner pstner;

	public MotesSendComdListHandler() {
		super();
		pstner = new MotesSendComdListPositioner();
	}

	public void resetCurrentPosition() {
		currentPosition = 0;
	}

	public void putComd(String comd) {
		if (comd.contains("reboot\r\n"))
			pstner.updateStartPoint(comdList.size());
		comdList.add(comd);
	}

	public String getComd(int signal) {
		//return signal == -1 ? comdList.get(pstner.rollBack()) : comdList.get(currentPosition++);
		String rt_value = null;
		if (signal == -1) {
			currentPosition = pstner.rollBack();
			rt_value = comdList.get(currentPosition);
			currentPosition++;
			MotesDebugger.print("CURRENT COMMAND IS " + rt_value + ".",
					MotesDebugger.debugOrNot);
			return rt_value;
		} else {
			rt_value = comdList.get(currentPosition);
			currentPosition++;
			MotesDebugger.print("CURRENT COMMAND IS " + rt_value + ".",
					MotesDebugger.debugOrNot);
			return rt_value;
		}
	}

	// calculate the length of command list
	public int comdListLength() {
		return comdList.size();
	}

	public void rollBack() {
		currentPosition = pstner.rollBack();
	}

	public void initComdList() {
		MotesDebugger.print("INIT COMD LIST FOR SCANNER.",
				MotesDebugger.debugOrNot);
		this.putComd("$$$");
		this.putComd("\r\n");
		this.putComd("factory R\r\n"); // reset module into default mode
		this.putComd("set wlan join 0\r\n"); // set join mode as 0
		this.putComd("set ip dhcp 1\r\n"); // set dhcp mode as 1
		this.putComd("save\r\n"); // save all configuration
		this.putComd("reboot\r\n"); // reboot module
	}

	public void resetOffset() {
		this.currentPosition = this.pstner.rollBack();
	}	
}
