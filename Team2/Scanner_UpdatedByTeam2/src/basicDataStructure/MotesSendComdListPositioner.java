//Used as pointer of ComdListHandler

package basicDataStructure;

public class MotesSendComdListPositioner {
	
	public int startPoint;
	int[] maintainAry;
	int index = 0;

	MotesSendComdListPositioner() {
		
		maintainAry = new int[100];
		maintainAry[this.index] = 0;
		startPoint = 0;
	}

	void updateStartPoint(int _currentStartPoint) {

		this.index++;
		maintainAry[this.index] = _currentStartPoint;

	}

	public int rollBack() {

		MotesDebugger.print("MAINTAINARY VALUE IS " + maintainAry[this.index], MotesDebugger.debugOrNot);
		//return index != 0? maintainAry[this.index--]:maintainAry[this.index];
		return this.startPoint != this.index? maintainAry[this.startPoint++] : maintainAry[this.index];

	}
	
}
