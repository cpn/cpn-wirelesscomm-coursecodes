// Used for printing debug information in different style

package basicDataStructure;

public class MotesDebugger {

	public static int debugOrNot = 1;

	public static void print(String info, int debugOrNot) {
		if (debugOrNot == 1) {
			// System.out.println("<---------------------------------->");
			System.out.println(info);
			System.out.println("<---------------------------------->");
		}

	}

	public static void print(String info) {
		if (debugOrNot == 1) {
			System.out.println(info);
		}
	}
}