package mainApp;

import java.util.concurrent.Semaphore;

/*
 * Saving all signal variables and locks
 * Definition following the form of: variables + lock + condition
 */
public class Signals {
	
	protected static int signal = 0; //control whether should command feeder roll back
	protected static Semaphore clntRcvd = new Semaphore(0); 									 //semaphore variable for the lock
	
	public static void signalInit(){
		Signals.clntRcvd_P();
	}
	
	// Function for setting and getting current mode
	
	public static synchronized int getSignal(){
		return signal;
	}
	
	/*
	 * Two functions for getting semaphore 
	 */
	public static void clntRcvd_P() {
		try {
			clntRcvd.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void clntRcvd_V() {
		clntRcvd.release();
	}
}
