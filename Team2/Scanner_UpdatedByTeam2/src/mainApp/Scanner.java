//main app

package mainApp;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import serialOps.MotesSerialParas;
import serialOps.MotesSerialReader;
import serialOps.SerialOp;
import basicDataStructure.MotesDebugger;
import basicDataStructure.MotesSendComdListHandler;

public class Scanner {
	protected static CommPortIdentifier targetPort;
	protected static SerialPort serialId;
	static InputStream serialIn;
	static OutputStream serialOut;
	static String str1 = new String();

	static private MotesSendComdListHandler ch;
	static String tmp = null;
	static String str = null;
	static String outFilePath = null;

	// Find comm port with name ttyUSB*
	protected static boolean findCommPort() {
		try {
			Enumeration<?> portList = CommPortIdentifier.getPortIdentifiers();
			while (portList.hasMoreElements()) {
				CommPortIdentifier portId = (CommPortIdentifier) portList
						.nextElement();
				System.out.println(portId.getName());
				if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					if (portId.getName().contains("ttyUSB")) {
						targetPort = CommPortIdentifier
								.getPortIdentifier(portId.getName());
						return true;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("No target port was found.");
		return false;
	}

	// configure parameters of comm port
	protected static void configCommPort(SerialPort serialId, String _str)
			throws Exception { // should add event listener
		serialId = (SerialPort) targetPort.open("CommEngine", 5000);
		serialId.setSerialPortParams(MotesSerialParas.baudRate,
				MotesSerialParas.dataBits, MotesSerialParas.stopBits,
				MotesSerialParas.parity);
		serialId.setFlowControlMode(MotesSerialParas.flowControl);

		serialIn = serialId.getInputStream();
		serialOut = serialId.getOutputStream();

		MotesSerialReader apWriter = new AccessPointWriter(serialIn,
				outFilePath);
		serialId.addEventListener(apWriter);
		serialId.notifyOnDataAvailable(true);
	}

	// configure serial port
	static boolean configSerialPort() throws Exception {
		if (!findCommPort()) {
			return false;
		}
		configCommPort(serialId, str1);
		return true;
	}

	// initialization procedure - including ComdListHandler initialization
	static boolean initial() {
		try {
			if (!configSerialPort()) { 
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		ch = new MotesSendComdListHandler();
		ch.initComdList();
		return true;
	}

	// search for access point - will result in an AP list
	static void findAccessPoints() {
		// SerialOp.SdData_NoWait("scan\r\n", serialOut);
		SerialOp.SdData_NoWait("scan\r\n", serialOut);
	}

	// main function
	public static void main(String args[]) {
		if (args.length < 3) {
			System.out.println("Length is " + args.length);
			System.err.println("Program - Required params: <outfile> <seconds> <times>");
			return;
		}
		outFilePath = args[0];
		int sleepSeconds = Integer.parseInt(args[1]);
		int times = Integer.parseInt(args[2]);

		int i = 0;

		if (!initial()) {
			System.err.println("Failed to initialize USB Wifi module.");
		}

		while (true) {
			while (ch.currentPosition < ch.comdListLength()) {
				try {
					if (MotesSerialReader.getRollBack() != 0) {
						ch.rollBack();
						MotesSerialReader.resetRollBack();
					}
					tmp = ch.getComd(Signals.getSignal());
					MotesDebugger.print("THE COMMAND NEEDED TO BE ENTERED IS "
							+ tmp + " AND CURRENT POSITION IS "
							+ ch.currentPosition, MotesDebugger.debugOrNot);
					SerialOp.SdData(tmp, serialOut, 3);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (ch.currentPosition == ch.comdListLength()) {
				SerialOp.SdData("$$$", serialOut, 3); // enter into command mode
				while (i < times) {
					i++;
					//System.out.println("" + i);
					findAccessPoints();

					// you could add some other mechanism to trigger another
					// scan I have one in my application while it is a little
					// complex so I remove it in this test program
					try {
						Thread.sleep(sleepSeconds * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					if(i == times)System.exit(0);
				}
				
				
			}
		}
	}
}
