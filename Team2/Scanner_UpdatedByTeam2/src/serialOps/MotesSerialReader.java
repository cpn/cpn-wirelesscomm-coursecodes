//Used for reading data from serial port when triggered

package serialOps;

import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import java.io.InputStream;

import mainApp.Signals;
import basicDataStructure.MotesDebugger;

public class MotesSerialReader implements SerialPortEventListener {
	
	InputStream in;
	String buffer;

	public MotesSerialReader(InputStream _in, String _buffer) {
		super();
		this.in = _in;
		this.buffer = _buffer;
	}

	public synchronized static int getRollBack() {
		return MotesReceiveDataAnalyzer.getRollBack();
	}

	public synchronized static void resetRollBack() {
		MotesReceiveDataAnalyzer.resetRollBack();
	}

	public String spread() {
		StringBuilder sb = new StringBuilder();
		try {
			while (in.available() > 0) {
				sb.append((char) in.read());
				Thread.sleep(10);
			}
			MotesDebugger.print("STRING RECEIVED IS " + sb.toString() + ".",
					MotesDebugger.debugOrNot);
		} catch (Exception e) {
		}
		return sb.toString();
	}

	protected void scannerReader() {
		buffer = spread();
		int rtValue = MotesReceiveDataAnalyzer.alyData(buffer);

		if (rtValue == -1) {
			MotesReceiveDataAnalyzer.setRollBack();
			MotesDebugger.print("[CLNTMODEREADER]: RETURN CMD ILLEGAL.",
					MotesDebugger.debugOrNot);
		}
		Signals.clntRcvd_V();
	}

	@Override
	public void serialEvent(SerialPortEvent arg0) {
		// TODO Auto-generated method stub

		// checking out event type
		switch (arg0.getEventType()) {
		case SerialPortEvent.BI: {
			MotesDebugger.print("SERIAL PORT EVENT BI",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.OE: {
			MotesDebugger.print("SERIAL PORT EVENT OE",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.FE: {
			MotesDebugger.print("SERIAL PORT EVENT FE",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.PE: {
			MotesDebugger.print("SERIAL PORT EVENT PE",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.CD: {
			MotesDebugger.print("SERIAL PORT EVENT CD",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.CTS: {
			MotesDebugger.print("SERIAL PORT EVENT CTS",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.DSR: {
			MotesDebugger.print("SERIAL PORT EVENT DSR",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.RI: {
			MotesDebugger.print("SERIAL PORT EVENT RI",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.OUTPUT_BUFFER_EMPTY: {
			MotesDebugger.print("SERIAL PORT EVENT OUTPUT_BUFFER_EMPTY",
					MotesDebugger.debugOrNot);
			break;
		}

		case SerialPortEvent.DATA_AVAILABLE:
			MotesDebugger.print("DATA_AVAILABLE.", MotesDebugger.debugOrNot);
			scannerReader();
		}
	}
}
