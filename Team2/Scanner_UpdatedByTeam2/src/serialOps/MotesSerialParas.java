//Used for containing parameters in serial port configuration 

package serialOps;

import gnu.io.SerialPort;

public class MotesSerialParas {
	public static final int baudRate = 9600;
	public static final int dataBits = SerialPort.DATABITS_8;
	public static final int stopBits = SerialPort.STOPBITS_1;
	public static final int parity = SerialPort.PARITY_NONE;
	public static final int flowControl = SerialPort.FLOWCONTROL_NONE;
}
