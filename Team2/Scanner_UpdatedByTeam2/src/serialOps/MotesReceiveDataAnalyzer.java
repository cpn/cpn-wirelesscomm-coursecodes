//Used for analyzing received data

package serialOps;

import java.io.BufferedReader;

public class MotesReceiveDataAnalyzer {
	protected static BufferedReader str_in;
	protected static String tmp;

	protected static String msg;

	protected static int rollbackOrNot = 0;

	protected static int scanInfo = 0;
	protected static int noApInfo = 0;

	protected static int joinInfo = 0;
	protected static int joinOrNot = 0;

	protected static String str;
	public static String str1;
	protected static int closeOrNot;

	protected static int connectedInfo = 0;

	// following three functions: control whether should program be rolled-back
	public synchronized static int getRollBack() {
		return rollbackOrNot;
	}

	public synchronized static void setRollBack() {
		rollbackOrNot = 1;
	}

	public synchronized static void resetRollBack() {
		rollbackOrNot = 0;
	}

	/*
	 * Check out returned data for useful information
	 */
	protected static int checkRecvData() {
		if (str.contains("SCAN FAILED") || str.contains("=FAILED")) {
			return -1;
		} else if (str.contains("$$$") || str.contains("CMD")
				|| str.contains("AOK") || str.contains("<2.45>")
				|| str.contains("Reboot")) {
			return 0;
		}
		return 0;
	}

	// Analyze data received
	public static int alyData(String _str) {
		str = new String(_str);
		str1 = new String(_str);
		return checkRecvData();
	}
}
	