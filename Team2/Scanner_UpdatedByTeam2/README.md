# Wifi Access Point Scanning

## How to run this program.

# --------------------------UPDATED AT SEP.30TH

*Make sure the USB Wifi Module is connected the Beaglebone Board before power the board on.*

 1. Follow the instruction from the [Course Wiki](https://bitbucket.org/cpn/cpn-wirelesscomm-coursecodes/wiki/Home) to install Ubuntu/OpenJDK-7/RXTXComm.jar on the beaglebone.

 2. Download the package on Beaglebone.

	```wget https://bitbucket.org/cpn/cpn-wirelesscomm-coursecodes/downloads/apscanner.tar.gz -O - | tar -xz```

 3. Run the `wifiscan` script with `sudo`

    ```sudo ./wifiscan <outfile> <period> <times>```

   This script stores the WIFI Access Point data to the `<outfile>`. You need
   also specify the `<period>` (seconds) for the scanning interval. The
   `period` value is suggest to be larger than 10s.

  <times> meams that after this many iteration, application will stop automatically.

You are encouraged to `git clone` this repository and modify the codes. *Pull requests are welcome*.
