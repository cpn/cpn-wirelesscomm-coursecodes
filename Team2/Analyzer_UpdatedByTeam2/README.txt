# This program is used for calculating RSS average value, 
# and it could also provide statistics like totally how many
# samples are gathered in the whole group of files (this 
# division is based on mac address), and the file sequence in
# which corresponding record is in.

# How to use it
#
# Firstly if you have set of files to process for one single 
# experiment, please rename them according rule like "Schorr-
# Center1", "SchorrCenter2" and so forth.
# Then open java file called Analyzer.java, and replace code
#
# fileName = new String("/home/ybai/Downloads/SchorrCenter/" + "SchorrCenter" + # nameShift);
#
# with corresponding absolute path to your measurement files
# and change SchorrCenter with prefix of name (number is not 
# included.)
# Finally replace all "SchorrCenter" in the java file with prefix
# you have selected.

# How to read result
#
# After a short wait, you should find display like:
#
# FILE SQ: SchorrCenter21,22,23,24,25
# SSID: Wifly_2	
# MAC: 00:06:66:80:bf:ac	
# RSS Value All: 48
# -47 -45 -46 -47 -48 -47 -47 -47 -46 -45 -45 -44 -46 -43 -43 -44
# -44 -44 -43 -44 -54 -54 -55 -55 -55 -54 -54 -59 -57 -57 -54 -55 
# -59 -59 -56 -55 -54 -55 -55 -62 -61 -60 -60 -60 -61 -60 -62 -63 
# RSS Value Avg: 51
# -46 -44 -55 -55 -61 
# 
# 1. First line shows that for mac address 00:06:66:80:bf:ac it 
# appears in file SchorrCenter21, SchorrCenter22, SchorrCenter23, 
# SchorrCenter24, SchorrCenter25.
# 2. Second line and third line shows SSID and corresponding MAC
# address.
# 3. RSS Value All shows all samples in these files corresponding
# to this MAC address. (Please ignore the first non-negative number)
# 4. RSS Value Avg calculates the average RSS value for each file 
# (values here are mathematical average ones, not exponential-then-log
# ones.)

# How to save result into file
# Copy & Paste ;)

# Any question
# ybai@cse.unl.edu




