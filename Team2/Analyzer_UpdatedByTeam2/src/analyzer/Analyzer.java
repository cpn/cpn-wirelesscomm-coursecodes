package analyzer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import basicDataStructure.DataTuple;

public class Analyzer {
	
	public static DataTuple[] list = new DataTuple[10000];	//Used for storing all data tuples extracted from sampling files
	public static int offset = 0; 							//current offset of list - start from 0
	public static int nameShift = 1;
	public static int index_avg = 0;
	
	
	/*
	 * getRSS value - Used to get RSS value from raw data
	 * return String-type RSS value
	 */
	public static String getRSS(String rawData){
		
		String tmp = new String(rawData.substring(6, 9));
		//System.out.println(tmp + "+++++");
		
		return tmp;
		
	}
	
	/*
	 * getMacAddress - Used to get MAC address from raw data
	 * return MAC Address String
	 */
	public static String getMacAddress(String rawData){
		
		String tmp = new String(rawData.substring(rawData.indexOf(":")-2, rawData.lastIndexOf(":")+3));
		//System.out.println(tmp + "-----");
		
		return tmp;
	}
	
	/*
	 * getSSID - Used to get MAC address from raw data
	 * return MAC address String
	 */
	public static String getSSID(String rawData){
		
		String tmp = new String(rawData.substring(rawData.lastIndexOf(",")+1, rawData.length()));
		//System.out.println(tmp + "*****");
		
		return tmp;	
	}
	
	/*
	 * getNewAvg - Used to get RSS Average Value
	 * return average value
	 */
	public static int getNewAvg(String rawData, int _tmp){
		
		//System.out.println("*" + list[_tmp].index);
		
		int up = 0;
		int down = 0;
		
		for(int i = list[_tmp].last_index; i < list[_tmp].index; i++)up = up + list[_tmp].rssValueAll[i];
		down = list[_tmp].index - list[_tmp].last_index + 1;
		
		//System.out.println("Up:" + up);
		//System.out.println("Down: " + down);
		
		int tmp = (up+Integer.parseInt(getRSS(rawData)))/down;
		
		//int tmp = (list[_tmp].rssValueAvg[index_avg]*list[_tmp].index+Integer.parseInt(getRSS(rawData)))/(list[_tmp].index+1);
		//System.out.println("" + tmp + "#####");
		
		list[_tmp].index = list[_tmp].index + 1;
		//System.out.println("" + list[_tmp].index + "^^^^^");
		
		return tmp;
	}
	
	/*
	 * checkExistence - Used for checking out that whether given MAC address exists in the list 
	 * return offset if found, -1 if not
	 */
	public static int checkExistence(String macAddress){
		
		int tmp = 0;
		while(tmp < offset){
			
			if(macAddress.contentEquals(list[tmp].mac)){
					//System.out.println("EXIST: " + tmp);
					return tmp;
			}
			else {
					
					tmp++;
			}
			
		}
		
		//System.out.println("NON-EXIST");
		return -1;
	}
	
	public static void getAllInfo_NonExist(String rawData){
		
		list[offset] = new DataTuple(); 
		
		list[offset].fileName = new String("BurnettHall" + nameShift);
		
		list[offset].mac = new String(getMacAddress(rawData));
		//System.out.println(list[offset].mac);
		
		list[offset].rssValueAvg[index_avg] = getNewAvg(rawData, offset);
		//System.out.println("" + list[offset].rssValueAvg);
		
		list[offset].rssValueAll[list[offset].index-1] = Integer.parseInt(getRSS(rawData));
		
		list[offset].SSID = new String(getSSID(rawData));
		
		offset++;
		
	}
	
	public static void getAllInfo_Exist(String rawData, int tmp){
		
		if(nameShift < 10){
		list[tmp].fileName = list[tmp].fileName.substring(list[tmp].fileName.length()-1).contentEquals(""+nameShift) 
				? list[tmp].fileName : list[tmp].fileName.concat("," + nameShift);
		}
		
		else if(nameShift >= 10 && nameShift < 100){
			
			list[tmp].fileName = list[tmp].fileName.substring(list[tmp].fileName.length()-2).contentEquals(""+nameShift) 
					? list[tmp].fileName : list[tmp].fileName.concat("," + nameShift);
			
		}
		
		list[tmp].rssValueAvg[index_avg] = getNewAvg(rawData, tmp);
		
		list[tmp].rssValueAll[list[tmp].index-1] = Integer.parseInt(getRSS(rawData));
		
	}
	
	public static void main(String args[]){
	
		FileReader frd;
		File f;
		String fileName = null;
		
		BufferedReader br;
		String tmp;
		int x;
		
		fileName = new String("/home/ybai/Downloads/BurnettHall/" + "BurnettHall" + nameShift);
		f = new File(fileName);
		
		System.out.println("------------------");
		while(f.exists()){
			
			try {
				
				frd = new FileReader(fileName);
				
				br = new BufferedReader(frd);
				
				try {
					while((tmp = br.readLine()) != null){
						
						if(!tmp.contains(","))continue;
						
						else if(tmp.contentEquals("\n"))continue;
						
						else if((x = checkExistence(getMacAddress(tmp))) != -1){
							
							//list[x].print();
							getAllInfo_Exist(tmp, x);
							
						}
						
						else if(checkExistence(getMacAddress(tmp)) == -1){
							
							getAllInfo_NonExist(tmp);
							//x = checkExistence(getMacAddress(tmp));
							//list[x].print();
						}
		
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//System.out.println("SchorrCenter" + Analyzer.nameShift);
			//for(int i = 0; i < offset; i++)list[i].print();
			//System.out.println("------------------");
			
			nameShift++;
			index_avg = nameShift;
			for(int i = 0; i < offset; i++){
				
				list[i].last_index = list[i].index;
				
			}
			
			
			fileName = new String("/home/ybai/Downloads/BurnettHall/" + "BurnettHall" + nameShift);
			f = new File(fileName);
			
		}
		
		System.out.println("******************");
		for(int i = 0; i < offset; i++)list[i].print();
		System.out.println("******************");
		
	}
	
	
}
