package basicDataStructure;

import analyzer.Analyzer;

public class DataTuple {
	
	public String SSID = null;
	public String mac = null;
	public String fileName = null;
	public int[] rssValueAll = new int[10000];
	public int index = 0;
	public int last_index = index;
	public int[] rssValueAvg = new int[10000];
	

	public DataTuple(){
		
		super();
		
	}
		
	public void print(){
		
		System.out.println("FILE SQ: " + fileName);
		System.out.println("SSID: " + SSID + "	");
		System.out.println("MAC: " + mac + "	");
		System.out.println("RSS Value All: " + this.index);
		for(int i = 0; i < this.index; i++)System.out.print(rssValueAll[i] + " ");
		System.out.println();
		System.out.println("RSS Value Avg: " + Analyzer.index_avg);
		for(int i = 0; i <= Analyzer.index_avg; i++)
			{
				if(rssValueAvg[i] == 0)continue;
				else System.out.print("" + rssValueAvg[i] + " ");
			}
		System.out.println();
		System.out.println("------------------");
	}
	
	
}
