package mainApp;

import gnu.io.SerialPortEvent;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import basicDataStructure.MotesDebugger;
import serialOps.MotesSerialReader;

/**
 * Writes access points to a text file.
 *
 */
public class AccessPointWriter extends MotesSerialReader {
	
	private static String buffer = new String();
	private FileOutputStream outFile;
	
	static public String getCurrentTimeString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public AccessPointWriter(InputStream inStream, String filepath) 
			throws FileNotFoundException {
		super(inStream, buffer);
		outFile = new FileOutputStream(filepath, false);
	}
	
	@Override
	public void serialEvent(SerialPortEvent arg0) {
		System.out.println("Serial Event: " + arg0.getEventType());
		String content = null;
		switch (arg0.getEventType()) {
		case SerialPortEvent.DATA_AVAILABLE:
			MotesDebugger.print("DATA_AVAILABLE.", MotesDebugger.debugOrNot);
			content = spread();
		}
		if (content != null && content.contains("SCAN:Found")) {
			// Write string to file
			try {
				// System.out.print(content);
				outFile.write(getCurrentTimeString().getBytes());
				outFile.write("\n".getBytes());
				outFile.write(content.getBytes());
				outFile.flush();
			} catch (IOException e) {
				System.err.println("Failed to write WIFI content to file.");
				e.printStackTrace();
			}
		} else {
			super.serialEvent(arg0);
		}
	}
}
