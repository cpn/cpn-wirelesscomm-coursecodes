/*  
	Author: Team 3
	CSE 496 -- Wireless Communication
	Assignment 1 
*/

#include<stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define adsize 18

typedef struct mrmnt measures;
struct mrmnt{
 	int measr;
 	measures *next;
};
typedef struct avrg avg;
struct avrg{
 	int avg_file;
 	int file_num;
 	avg *next;
};

typedef struct macnode mac; 
struct macnode{
	char mac_adr[adsize];
 	measures *mh;
	avg *avgh;
	int count;
 	mac *next;
};

mac *mach; 
int n_scans;

void printAvgs (FILE *foa);
void Printlist (FILE *fo, int f_num);
mac *searchlist (char add[adsize]);
void InsertMac ( mac *new_node );
measures *InsertMsr ( measures *list_head, int msr);
void Macslist (FILE *fi);
avg *InsertAvg ( avg *list_head, int avgVal, int f_num);

int main (){
	FILE *fi;
	FILE *fo;
	int num_files;
	int k, j; 
	char num[20]; 
	
	mach = NULL;
	
	printf("# files: ");
	scanf("%d", &num_files);
	
	//Iterates over the list of files in the folder and creates the linked list of 
	//Mac addresses and their average received signal strength for each measurement
	for (k=0; k<num_files; k++){
		j=k;
		itoa(j, num, 10);
		//You may want to change the name of the input file 
		//(please, refer to read-me.txt)
		if((fi = fopen(strcat(num, ".txt"), "r"))== NULL){
			printf("Input file problem!\n"); 
		}else{
			Macslist (fi);
			//You may want to change the name of the output file
			// (please, refer to read-me.txt)	
			if((fo = fopen(strcat(num, "_res.txt"), "w"))== NULL){
				printf("Output file problem!\n"); 
			}else{
				Printlist(fo, k);
			}
		}
	}
	//output the averages to a text file named "avgs.txt"
	if((fo = fopen("avgs.txt", "w"))== NULL){
		printf("Output file problem!\n"); 
	}else{
		printAvgs(fo);
	}
	return 0; 
}

// Computes the averages of the signal strengths received for each Mac Address in
//each file and outputs the value to the screen and to a file
void printAvgs (FILE *foa){
	mac *iter=mach;
	avg *miter, *max; 
	int count; 
	
	while (iter!=NULL){
		count = 0; 
		printf("Mac address: %s \n	", iter->mac_adr);
		fprintf(foa, "Mac address: %s \n	", iter->mac_adr);
		miter = iter->avgh;
		max = iter->avgh; 
		while (miter != NULL){
			if (max->avg_file < miter->avg_file){
				max = miter; 
			}
			printf(" %d (%d)", miter->avg_file, miter->file_num);
			fprintf(foa, " %d (%d)", miter->avg_file, miter->file_num);
			miter = miter->next;
			count++;
		}
		printf("\n  For %d measurements\n    -- Max value of %d in file %d\n\n",
		 count, max->avg_file, max->file_num);
		fprintf(foa, "\n  For %d measurements\n    -- Max value of %d in file %d\n\n",
		 count, max->avg_file, max->file_num);		 
		iter = iter->next;	
	}
}

//Prints the list of Mac addresses along with their measurements and averages 
//to a text file   
void Printlist (FILE *fo, int f_num){
	mac *iter=mach;
	measures *miter; 
	int count, sum; 
	
	while (iter!=NULL){
		count = 0; sum =0;
		//printf("Mac address: %s \n	", iter->mac_adr);
		fprintf(fo, "Mac address: %s \n	", iter->mac_adr);
		miter = iter->mh; 		
		while (miter != NULL){
			//printf(" %d ", miter->measr);
			fprintf(fo, " %d ", miter->measr);
			count ++; 
			sum += miter->measr; 
			miter = miter->next;
		}
		if (count != 0){
			sum = sum/count; 
			iter->avgh = InsertAvg (iter->avgh, sum, f_num);
			//printf("\n	 The avg is: %d (for %d measurements)\n\n", sum, count);
			fprintf(fo, "\n	 The avg is: %d (for %d measurements)\n\n", sum, count);
			iter->mh = NULL;
		}
		iter = iter->next;	
	}
}

// Searches for a Mac address in the list of Mac addresses
mac *searchlist (char add[adsize]){
	mac *iter; 
	mac *result; 
	int len;
	char imac[adsize], nmac[adsize];
	iter = mach;

	while (iter!=NULL){
		len = strlen(iter->mac_adr);
		strncpy(imac, iter->mac_adr, len - 3);
		imac[len - 3] = 0;
		strncpy(nmac, add, len - 3);
		nmac[len - 3] = 0;
		if (strcmp(imac, nmac) == 0){
			result = iter;  
		}
	    iter = iter->next;
	}		
	return result; 
}

void InsertMac ( mac *new_node ) 	{
	new_node->next = mach;
	mach = new_node;
}

measures *InsertMsr ( measures *list_head, int msr) 	{
	measures *new_node;
	new_node = (measures *) malloc ( sizeof ( measures) ); 
	new_node->measr = msr; 
	new_node->next = list_head;
	list_head = new_node;
	return list_head;
}

avg *InsertAvg ( avg *list_head, int avgVal, int f_num) 	{
	avg *new_node;
	new_node = (avg *) malloc ( sizeof ( avg) ); 
	new_node->avg_file = avgVal; 
	new_node->file_num = f_num;
	new_node->next = list_head;
	list_head = new_node;
	return list_head;
}

//Iterates over the input file and retrieves the data about the signal strength 
void Macslist (FILE *fi) {
	char c;
	int j, i, val;
	char *token;
	char str[100];
	int token_number = 0;
	char macadd[18];
	mac *new_entry; 
	
	fgets (str, 99, fi);
	while(!feof(fi)){	
		token = (char *) strtok( str, " " );
		if (token != NULL){
			if (strcmp(token, "SCAN:Found") == 0){
				token = (char *) strtok( NULL, " " );
				n_scans = atoi(token);
				for (i=0; i<n_scans; i++){
					fgets (str, 99, fi);
					token = (char *) strtok( str, "," );			 
					token = (char *) strtok( NULL, "," );			 
					token = (char *) strtok( NULL, "," );
					val = atoi (token);
					for (j=0; j<5; j++){
						token = (char *) strtok( NULL, "," );
					}
					strcpy(macadd, token);
					new_entry = searchlist(macadd); 
					if (new_entry == NULL){
						new_entry = (mac *) malloc ( sizeof ( mac) );
						strcpy(new_entry->mac_adr, macadd); 
						new_entry->next = NULL;
						new_entry->mh = NULL;
						new_entry->avgh = NULL;  
						InsertMac(new_entry); 
						new_entry->mh = InsertMsr(new_entry->mh, val);
					}else{
						new_entry->mh = InsertMsr(new_entry->mh, val);
					}
				}	
			}
		}		
		fgets (str, 99, fi);	
	}
}
