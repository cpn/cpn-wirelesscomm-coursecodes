C=imread('Avary.png');%upload the map to matlab
figure, imshow(C);
hold on
A=[595, 218,-700;488,472,-700;] ;%position and signal strenght of each measurement location, the more the better
x=A(:,1);y=A(:,2);z=A(:,3);% x,y are the coordinates, z is the signal strength
scatter(x,y,2,z)%put the location on the map
hold on
[X,Y,Z]=griddata(x,y,z,linspace(20, 600)',linspace(100, 600),'v4');%calculate the coverage of the signal strength with data of discrete measure points
contour(X,Y,Z,35) %put the contour of the signal strength on the map
hold off