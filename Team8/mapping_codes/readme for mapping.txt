Firstly, you need put a map (you can use a screen shot) in the folder. I give an example map.
Matrix A contains the coordinates of measurement locations, and the signal strength of AP you want to map. The more locations, the clearer the map is. 
In the codes, I just give two random locations and signal strength.In the place with too week strength, you can give random small strength data.
The locations should be in coordinate of the screen shot map. You could measure them with data cursor tool in matlab according to the GPS locations you marked.
