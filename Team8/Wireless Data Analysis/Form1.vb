﻿Imports System.Windows.Forms.DataVisualization.Charting

Public Class Form1


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog()
        Dim strFileName As String, readings As Integer

        ofd.Title = "Open File Dialog"
        ofd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        ofd.FilterIndex = 2
        ofd.RestoreDirectory = True

        If ofd.ShowDialog() = DialogResult.OK Then
            strFileName = ofd.FileName
        End If
        If strFileName = "" Then
            Exit Sub
        End If
        Dim EntireFile As String

        Dim oRead As System.IO.StreamReader

        oRead = IO.File.OpenText(strFileName)
        EntireFile = oRead.ReadToEnd()
        ' Data1.Text = EntireFile
        Dim pStr As String = EntireFile
        Dim prev As Integer, a As Integer, b As Integer, SingleScan As String
        prev = 1
        'If SData.Items.Count > 0 Then
        '    If Not (MsgBox("Click Yes to Append, No to Clear", vbYesNo) = vbYes) Then
        '        SData.Items.Clear()
        '        SData.Refresh()
        '    End If
        'End If
        SData.Items.Clear()
        SData.Refresh()

        For i = 0 To EntireFile.Length
            Dim a1 As Integer

            a = InStr(prev, pStr, "SCAN:Found")
            If a = 0 Then
                Exit For
            Else
                readings = readings + 1
            End If
            b = InStr(a, pStr, "END:")
            prev = b
            SingleScan = pStr.Substring(a, b - a - 1)

            Dim Lines() As String
            Dim Params() As String

            Dim arg() As String = {vbCrLf, vbLf}
            Dim arg2() As String = {","}

            Lines = SingleScan.Split(arg, StringSplitOptions.None)

            For k As Integer = 0 To Lines.GetUpperBound(0)


                Params = Lines(k).Split(arg2, StringSplitOptions.None)
                If Params.Length > 1 Then
                    SData.Items.Add(Params(7) + " $$ " + Params(2))
                End If

            Next

        Next

        Read1.Text = "Number of readings= " + readings.ToString
        readings = 0

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CData.Items.Clear()


        Dim MacList As New List(Of String)()

        Dim sValues() As String, ind As Integer, count As Integer, sPower As Integer, cPower As Integer, tDataString As String
        Dim sValues1() As String
        Dim arg() As String = {"$$"}
        For i = 0 To SData.Items.Count - 1
            sValues = SData.Items(i).ToString.Split(arg, StringSplitOptions.None)
            sPower = sValues(1)
            If MacList.Contains(sValues(0).Trim) Then
                'cList.

                ind = MacList.IndexOf(sValues(0).Trim)
                sValues1 = CData.Items(ind).ToString.Split(arg, StringSplitOptions.None)
                CData.Items.RemoveAt(ind)
                CData.Refresh()
                count = Integer.Parse(sValues1(2)) + 1
                cPower = Integer.Parse(sValues1(1) + sPower) / 2
                CData.Items.Add(sValues1(0).Trim + " $$ " + cPower.ToString + " $$ " + count.ToString)

            Else
                CData.Items.Add(SData.Items(i) + " $$ 1")
                MacList.Add(sValues(0).Trim)
 
            End If
        Next

        Dim LineDataC() As String
        Dim LineDataT() As String

        Dim argu() As String = {"$$"}

        Dim TMacList As New List(Of String)()

        For z = 0 To TData.Items.Count - 1
            LineDataT = TData.Items(z).Split(argu, StringSplitOptions.None)
            TMacList.Add(LineDataT(0).Trim)
        Next

        For j = 0 To CData.Items.Count - 1
            LineDataC = CData.Items(j).Split(argu, StringSplitOptions.None)
            If TMacList.Contains(LineDataC(0).Trim) Then
                ind = TMacList.IndexOf(LineDataC(0).Trim)
                tDataString = TData.Items(ind).ToString
                TData.Items.RemoveAt(ind)
                TMacList.RemoveAt(ind)
                TData.Items.Add(tDataString + " $$ " + LineDataC(1))
                TMacList.Add(LineDataC(0).Trim)
            Else
                TData.Items.Add(LineDataC(0).Trim + " $$ " + LineDataC(1))

            End If


        Next



    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim s1 As String = String.Empty

        For i As Integer = 0 To CData.Items.Count - 1
            s1 &= CData.Items.Item(i) & Environment.NewLine
        Next

        Clipboard.SetText(s1)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        CData.Items.Clear()
        SData.Items.Clear()
        TData.Items.Clear()

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim a As String

        Dim sfd As SaveFileDialog = New SaveFileDialog()

        sfd.Title = "Open File Dialog"
        '  sfd.InitialDirectory = "C:\Users\Sonith\Documents\Wireless\Readings\2"
        sfd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        sfd.FilterIndex = 2
        sfd.RestoreDirectory = True

        If sfd.ShowDialog() = DialogResult.OK Then
            a = sfd.FileName
        End If


        If Not System.IO.File.Exists(a) = True Then
            Dim file As System.IO.FileStream
            file = System.IO.File.Create(a)
            file.Close()
        End If
        Dim s As String
        For i = 0 To CData.Items.Count - 1
            s = s + CData.Items(i).ToString + Environment.NewLine

        Next
        My.Computer.FileSystem.WriteAllText(a, s, False)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub



    Private Sub TData_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TData.SelectedIndexChanged
        Chart1.Series.Clear()
        Chart1.Titles.Clear()
        Chart1.Titles.Add("Average Power Chart")

        Dim ind As Integer



        For i = 0 To TData.SelectedItems.Count - 1

            Dim Powers() As String

            Dim arg() As String = {"$$"}

            Powers = TData.SelectedItems(i).ToString().Split(arg, StringSplitOptions.None)

            Dim s As New Series
            s.Name = Powers(0)
            s.ChartType = SeriesChartType.Line

            For k As Integer = 1 To Powers.GetUpperBound(0)
                s.Points.AddY(Powers(k))
            Next
            Chart1.Series.Add(s)





        Next


    






        'ind = TData.SelectedIndex
        'If ind >= 0 Then
        '    Dim Powers() As String

        '    Dim arg() As String = {"$$"}

        '    Powers = TData.Items(ind).Split(arg, StringSplitOptions.None)

        '    Dim s As New Series
        '    s.Name = Powers(0)
        '    s.ChartType = SeriesChartType.Line

        '    For k As Integer = 1 To Powers.GetUpperBound(0)
        '        s.Points.AddY(Powers(k))
        '    Next
        '    Chart1.Series.Add(s)
        'Else
        '    Chart1.Series.Clear()
        '    Chart1.Titles.Clear()
        'End If
    End Sub


End Class


