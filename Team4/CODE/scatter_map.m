X=[-96.703857 -96.70359 -96.703522 1] % Longitud values
Y=[40.818211 40.818241 40.818096 1] % Latitud values
Z=[-78 -76.53846154 -61.92857143 -1] %Power values

Z=(Z+50) % The values are raised 50dB so the circles in scatter can be seen

% Creates a figure, uses scater to plot it and selects the type of colormap
figure % Creates a figure  
scatter(X,Y,Z*(-10),Z,'fill') % Configure scatter to map the plot
colormap(summer) % Sets the colors used in scatter to 'summer'


