Matlab code used for create the plots.

The values of Longitud, Latitud and Power should be set in the vectors.
Once the code is run, a plot will be seen.
In this plot each point is locate according to its coordinates and the radius of point
and its color depends on the power obtained in the measurement.