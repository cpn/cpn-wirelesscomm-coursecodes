Readme File from Team4.
-----------------------

In order to process the raw data (./apdata* folders), we used Microsoft Excel's Pivot Tables. The steps used are:

1. Open the raw data file with Excel.
2. Delete all the rows that do not contain relevant data.
3. Use function Text to Columns, using "," as the delimiter.
4. Insert two columns in the beginning of the data and type the GPS coordinates (use auto complete)
5. Use function "LEFT()" to get the root of the mac address (we call the root as the first 15 characters) (use autocomplete for
all the rows)
5. Insert a row at the top of the data and name each column.
6. Select all the data and call the function Pivote Table.
7. Use the pivot table wizard to select the relevant information. In our case we used the root, latitud, longitud and power and
we arranged it in a way we could see all the locations (with the power signal received) for each root.

---------------------------

In the pivot table all the AP with the same root will be grouped under a cell , and the GPS location and power will be under 
each of the cells.

We used the average of the power to determine the power measured in each GPS location for each AP.

With this information, it is simple to plot in Matlab as well as mark the location in Google Earth.

After we have the plot from Matlab and the locations in Google Earth, the plot can be imported to Google Earth and combine to 
see the power of the signal received at each location.

In our case, we only mapped and analyzed the AP that were found in 3 or more locations. These AP were grouped according to
their tentative location and then identified.