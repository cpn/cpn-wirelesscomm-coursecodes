% CSCE 896 Assignment Project 1 shared source code to demo how to plot
% google map on your figure
% Author: Zhongyuan Zhao (zhzhao@cse.unl.edu)
% version 1.0
% Feature: when you zoom in, zoom out or re-size the figure, the map will
% automatically be reloaded.
% dependent file: plot_google_map.m
clear all
close all

load('samples.mat'); % load demo measurement data
hrss = figure;
% plot measurement data on figure
scatter(samples(:,1),samples(:,2),20,samples(:,3),'o','fill');
% plot google_map on figure
plot_google_map('maptype','roadmap');
% limit the axis and color scale
xlim([-96.706713 -96.699911]);
ylim([40.817236 40.820407]);
maxrss = max(samples(:,3));
caxis([-100 ceil(maxrss)])
% display colorbar
colorbar
% set title of figure
title('Team 1 measurements for an UNL-AIR Access Point');
% save figure
saveas(hrss,'demo_google_map_in_your_figure.fig');