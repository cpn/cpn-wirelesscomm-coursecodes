package text;

import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ReadWriteRawFile {
    public static void writeFileByLine(String write)throws IOException{
    	String strPath = "src/text/scanner_data.txt";
    	File file = new File(strPath);
    	if (!file.exists()) {
    	    file.createNewFile();
    	   }
    	  
    	   
    	   FileWriter fileWriter = new FileWriter(strPath,true);

    	  
    	   String nextLine = System.getProperty("line.separator");
    	  
    	   fileWriter.write(nextLine+ write); 
    	   
    	   fileWriter.flush();
    	   fileWriter.close();
    	  
    	}
    
	

  
    public static void readFileByLines() {
    	String fileName = "src/text/scanner.txt";
        File file = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            String tempTime = null;
            
            while ((tempString = reader.readLine()) != null) {
                
            	if (tempString.indexOf("/", 0) >= 0){
                //System.out.println("line " + line + ": " + tempString);
                tempTime = tempString;
                }
            	if(tempString.indexOf(",", 0) >= 0&&tempTime!=null){
            		tempString=tempString+","+tempTime;
            		System.out.println(tempString);
            		writeFileByLine(tempString);
            	}
            	if(tempString.indexOf("END", 0) >= 0){
            		tempTime = null;
            	}
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
    }

    

    public static void main(String[] args) {
        ReadWriteRawFile.readFileByLines();
    }
    
}
